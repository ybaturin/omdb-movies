# English

## About project

The result of the task:
```
Need to create an application, which allows to search movies. A user should be able to remove or add a movie as a favorite.
For movie searching need to use API www.omdbapi.com or similar.
After a user clicked by a movie he has to be forwarded to a page of movie details.
Requirments: Typescript, Angular 5.
Also, would be nice :
- create an autocomplete to search movies
- save the list of favorites on the user computer
- use Angular Material
```
## To run on your local machine:

1. Install required dependencies
```
npm run install
```

2. Run dev version:
```angular2html
npm run start-dev
``` 

3. Go to page:
```
http://localhost:4200
```

# Russian doc

## О проекте 
Является результатом тестового задания: 
```
Сделать приложение, позволяющее искать фильмы, добавлять игх в список и удалять из него. 
Для поиска фильмов использовать API www.omdbapi.com или любое другое. 
Кликнув на любой фильм из списка, можно перейти на страницу с подробной информацией об этом фильме. 
Обязательные требования: - TypeScript; - Angular 5. 
Плюсом будет (также нужно выполнить): 
- Реализовать поиск автокомплитом; 
- Сохранять список на клиенте; 
- Использовать Angular Material.
```

## Для запуска на локальной машине

1. Установить пакеты 
```
npm run install
```

2. Запустить dev версию:
```angular2html
npm run start-dev
``` 

3. Перейти на страницу
```
http://localhost:4200
```
