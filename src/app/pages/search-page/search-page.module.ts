import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule, MatFormFieldModule, MatInputModule, MatPaginatorModule } from '@angular/material';
import { MovieCardModule } from 'src/app/ui/movie-card/movie-card.module';
import { SearchPageRoutingModule } from './search-page-routing.module';
import { SearchPageComponent } from './search-page.component';
import { SearchInputComponent } from './search-input/search-input.component';1

@NgModule({
  imports: [
    CommonModule,
    SearchPageRoutingModule,
    MatInputModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    MovieCardModule,
    MatPaginatorModule,
  ],
  declarations: [SearchPageComponent, SearchInputComponent]
})
export class SearchPageModule { }
