import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MovieDetails } from 'src/app/models/movie-details';
import { SearchResponse } from 'src/app/models/search-response';

const API_URL = '//www.omdbapi.com/?apikey=47d21732';

@Injectable({
  providedIn: 'root',
})
export class MoviesService {

  constructor(private http: HttpClient) {

  }

  getMovieDetails(imdbId: string): Observable<MovieDetails | undefined> {
    return this.http.get<MovieDetails>(`${API_URL}`, {
      params: {
        i: imdbId,
      },
    })
      .pipe(
        map((details) => {
          return details.Response === 'True' ? details : undefined;
        }));
  }

  search(name: string, pageIndex?: number): Observable<SearchResponse> {
    const page = pageIndex !== undefined ? String(pageIndex + 1) : undefined;
    return this.http.get<SearchResponse>(`${API_URL}`, {
      params: {
        page,
        s: name,
      },
    });
  }
}
