import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Movie } from 'src/app/models/movie';
import { StorageService, StorageTokenType } from 'src/app/services/storage.service';

@Injectable({
  providedIn: 'root',
})
export class FavoritesService {
  change$: Observable<void>;
  private favorites = new Map<string, Movie>();
  private changeSource = new Subject<void>();

  constructor(private storage: StorageService) {
    this.change$ = this.changeSource.asObservable();
    this.loadFromStorage();
  }

  get count(): number {
    return this.favorites.size;
  }

  getMovies(): Movie[] {
    return Array.from(this.favorites.values());
  }

  has(movie: Movie): boolean {
    return this.favorites.has(movie.imdbID);
  }

  toggle(movie: Movie): void {
    const id = movie.imdbID;
    if (this.has(movie)) {
      this.favorites.delete(id);
    } else {
      this.favorites.set(id, movie);
    }
    this.changeSource.next();
    this.saveToStorage();
  }

  private saveToStorage(): void {
    this.storage.setValue(StorageTokenType.favorites, this.getMovies());
  }

  private loadFromStorage(): void {
    const favorites = this.storage.getValue<Movie[]>(StorageTokenType.favorites) || [];
    favorites.forEach(movie => this.favorites.set(movie.imdbID, movie));
  }
}
