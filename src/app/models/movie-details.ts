import { Movie } from 'src/app/models/movie';

export interface MovieDetails extends Movie {
  Actors: string;
  Plot: string;
  Genre: string;
  Runtime: string;
  Response: string;
}
