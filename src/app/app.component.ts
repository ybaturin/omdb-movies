import { Component, ViewEncapsulation } from '@angular/core';
import { FavoritesService } from 'src/app/services/favorites.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less'],
  encapsulation: ViewEncapsulation.None,
})
export class AppComponent {
  constructor(private favorites: FavoritesService) {

  }

  get favoritesCount(): number {
    return this.favorites.count;
  }
}
