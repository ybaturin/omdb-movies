import { Component, OnDestroy, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { Movie } from 'src/app/models/movie';
import { MoviesService } from 'src/app/services/movies.service';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.less'],
})
export class SearchPageComponent implements OnInit, OnDestroy {
  readonly PAGE_SIZE = 10;
  movies?: Movie[];
  totalResults: number;
  movieName = '';
  currentPage = 0;
  private isLoading: boolean;
  private queryParamsSub: Subscription;

  constructor(
    private moviesService: MoviesService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
  }

  search(name: string) {
    this.movieName = name;
    this.currentPage = 0;
    this.saveSearchParamsToUrl();
    this.loadPartItems(true);
  }

  ngOnInit(): void {
    this.initSearchByUrl();
    this.queryParamsSub = this.route.queryParams.subscribe(() => {
      this.initSearchByUrl();
    });
  }

  ngOnDestroy(): void {
    this.queryParamsSub.unsubscribe();
  }

  onPageChanged($event: PageEvent) {
    this.currentPage = $event.pageIndex;
    this.saveSearchParamsToUrl();
    this.loadPartItems();
  }

  get noMovies(): boolean {
    return !this.isLoading && (!this.movies || this.movies.length === 0);
  }

  private initSearchByUrl(): void {
    const queryParams = this.route.snapshot.queryParams;
    this.movieName = queryParams.name || '';
    this.movies = undefined;
    if (!this.movieName) {
      return;
    }

    const currentPage = parseInt(queryParams.page, 10);
    this.currentPage = isFinite(currentPage) ? currentPage : 0;

    this.loadPartItems();
  }

  private saveSearchParamsToUrl(): void {
    this.router.navigate([], {
      queryParams: {
        name: this.movieName || undefined,
        page: this.currentPage || undefined,
      },
    });
  }

  private loadPartItems(reset: boolean = false) {
    if (reset) {
      this.currentPage = 0;
    }
    this.isLoading = true;
    this.moviesService.search(this.movieName, this.currentPage)
      .pipe(
        finalize(() => this.isLoading = false),
      )
      .subscribe((resp) => {
        this.movies = resp.Search || [];
        this.totalResults = resp.totalResults ? parseInt(resp.totalResults, 0) : 0;
      });
  }
}
