import { Movie } from 'src/app/models/movie';

export interface SearchResponse {
  Search: Movie[];
  totalResults: string;
}
