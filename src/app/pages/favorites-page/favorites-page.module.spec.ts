import { FavoritesPageModule } from './favorites-page.module';

describe('FavoritesPageModule', () => {
  let favoritesPageModule: FavoritesPageModule;

  beforeEach(() => {
    favoritesPageModule = new FavoritesPageModule();
  });

  it('should create an instance', () => {
    expect(favoritesPageModule).toBeTruthy();
  });
});
