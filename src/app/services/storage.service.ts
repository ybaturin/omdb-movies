import { Injectable } from '@angular/core';

export enum StorageTokenType {
  favorites = 'favorites',
}


@Injectable({
  providedIn: 'root'
})
export class StorageService {

  setValue(token: StorageTokenType, value: any): void {
    if (value === undefined) {
      localStorage.removeItem(token);
    } else {
      localStorage.setItem(token, JSON.stringify(value));
    }
  }

  getValue<T>(token: StorageTokenType): T | undefined {
    const result = localStorage.getItem(token);
    return result ? JSON.parse(result) : undefined;
  }
}
