import { HttpClientModule } from '@angular/common/http';
import { MatIconModule, MatRippleModule } from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { MoviesService } from 'src/app/services/movies.service';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    LoadingBarHttpClientModule,
    RouterModule,
    MatRippleModule,
    MatIconModule,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
