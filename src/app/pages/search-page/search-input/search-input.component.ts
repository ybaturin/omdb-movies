import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatAutocompleteTrigger } from '@angular/material';
import { Observable } from 'rxjs';
import { debounceTime, filter, map, switchMap, tap } from 'rxjs/operators';
import { Movie } from 'src/app/models/movie';
import { MoviesService } from 'src/app/services/movies.service';

const SEARCH_DELAY = 200;

@Component({
  selector: 'app-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.less'],
})
export class SearchInputComponent implements OnInit {
  @ViewChild('input', { read: MatAutocompleteTrigger }) autocompleteTrigger: MatAutocompleteTrigger;
  @Input() minSymbols = 3;

  @Input()
  get search(): string {
    return this.searchValue;
  }

  set search(value: string) {
    if (value !== this.searchValue) {
      this.searchCtrl.setValue(value, { emitEvent: false });
    }
  }

  @Output() searchChange = new EventEmitter<string>();

  searchCtrl = new FormControl('');
  movies = <Movie[]> [];
  private isLoading: boolean;

  constructor(private moviesService: MoviesService) {
  }

  ngOnInit(): void {
    this.searchCtrl.valueChanges
      .pipe(
        map((value) => {
          this.isLoading = true;
          this.movies = [];
          return value || '';
        }),
        filter(value => value.length >= this.minSymbols),
        debounceTime(SEARCH_DELAY),
        switchMap(val => this.searchMovies(val)),
      )
      .subscribe((movies) => {
        this.movies = movies;
        this.isLoading = false;
      });
  }

  onEnter(): void {
    this.autocompleteTrigger.closePanel();
    this.searchChange.next(this.searchValue);
  }

  get notEnoughSymbols(): boolean {
    return this.searchValue.length < this.minSymbols;
  }

  get isNothingFound(): boolean {
    return !this.notEnoughSymbols && !this.isLoading && this.movies.length === 0;
  }

  private get searchValue(): string {
    return this.searchCtrl.value || '';
  }

  private searchMovies(name): Observable<Movie[]> {
    return this.moviesService.search(name)
      .pipe(
        map(resp => resp.Search || []),
      );
  }

}
