import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MovieCardModule } from 'src/app/ui/movie-card/movie-card.module';

import { FavoritesPageRoutingModule } from './favorites-page-routing.module';
import { FavoritesPageComponent } from './favorites-page.component';

@NgModule({
  imports: [
    CommonModule,
    FavoritesPageRoutingModule,
    MovieCardModule,
    RouterModule,
  ],
  declarations: [FavoritesPageComponent],
})
export class FavoritesPageModule {
}
