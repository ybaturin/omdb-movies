import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule, MatRippleModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { MovieCardComponent } from './movie-card.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MatRippleModule,
    MatIconModule,
  ],
  declarations: [MovieCardComponent],
  exports: [MovieCardComponent],
})
export class MovieCardModule { }
