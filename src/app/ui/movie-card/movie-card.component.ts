import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Movie } from 'src/app/models/movie';
import { FavoritesService } from 'src/app/services/favorites.service';

@Component({
  selector: 'app-movie-card',
  templateUrl: './movie-card.component.html',
  styleUrls: ['./movie-card.component.less'],
})
export class MovieCardComponent {
  @Input() movie: Movie;
  @Input() hideTitle = false;

  constructor(private favorites: FavoritesService) {}

  get isFavorite(): boolean {
    return this.favorites.has(this.movie);
  }

  get hasPoster(): boolean {
    return this.movie.Poster && this.movie.Poster !== 'N/A';
  }

  onFavoriteClicked($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.favorites.toggle(this.movie);
  }

  onFavoriteMouseDown($event: MouseEvent) {
    $event.preventDefault();
    $event.stopPropagation();
  }
}
