import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Movie } from 'src/app/models/movie';
import { FavoritesService } from 'src/app/services/favorites.service';

@Component({
  selector: 'app-favorites-page',
  templateUrl: './favorites-page.component.html',
  styleUrls: ['./favorites-page.component.less'],
})
export class FavoritesPageComponent implements OnInit, OnDestroy {
  movies = <Movie[]> [];
  private favoritesChangeSub: Subscription;

  constructor(private favorites: FavoritesService) {
  }

  ngOnInit(): void {
    this.loadFavorites();
    this.favoritesChangeSub = this.favorites.change$.subscribe(() => this.loadFavorites());
  }

  ngOnDestroy(): void {
    this.favoritesChangeSub.unsubscribe();
  }

  private loadFavorites() {
    this.movies = this.favorites.getMovies();
  }
}
