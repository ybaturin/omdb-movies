import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { MovieDetails } from 'src/app/models/movie-details';
import { MoviesService } from 'src/app/services/movies.service';

@Component({
  selector: 'app-movie-page',
  templateUrl: './movie-page.component.html',
  styleUrls: ['./movie-page.component.less'],
})
export class MoviePageComponent implements OnInit {
  details?: MovieDetails;
  loaded = false;
  private queryParamsSub: Subscription;

  constructor(
    private moviesService: MoviesService,
    private route: ActivatedRoute,
  ) {
  }

  ngOnInit() {
    this.queryParamsSub = this.route.params.subscribe(() => {
      this.loadMovie();
    });
  }

  private loadMovie(): void {
    const id = this.route.snapshot.params.id;
    if (!id) {
      this.loaded = true;
      return;
    }

    this.moviesService.getMovieDetails(id)
      .subscribe((details) => {
        this.details = details;
        this.loaded = true;
      });
  }
}
