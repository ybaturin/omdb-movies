import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'search',
    pathMatch: 'full',
  },
  {
    path: 'search',
    loadChildren: 'src/app/pages/search-page/search-page.module#SearchPageModule',
  },
  {
    path: 'movie',
    loadChildren: 'src/app/pages/movie-page/movie-page.module#MoviePageModule',
  },
  {
    path: 'favorites',
    loadChildren: 'src/app/pages/favorites-page/favorites-page.module#FavoritesPageModule',
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [
    RouterModule,
  ],
})
export class AppRoutingModule {
}
