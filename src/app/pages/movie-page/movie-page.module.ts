import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MovieCardModule } from 'src/app/ui/movie-card/movie-card.module';

import { MoviePageRoutingModule } from './movie-page-routing.module';
import { MoviePageComponent } from './movie-page.component';

@NgModule({
  imports: [
    CommonModule,
    MoviePageRoutingModule,
    MovieCardModule,
  ],
  declarations: [MoviePageComponent]
})
export class MoviePageModule { }
